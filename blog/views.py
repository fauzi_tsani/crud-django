import datetime
from django.contrib.auth.models import User
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect,HttpResponse
from django.template import RequestContext, Context, loader
from CRUD.blog.models import *
from django.forms.formsets import formset_factory
from CRUD.blog.form import *
from django.core.context_processors import csrf

def index(request):

	return render_to_response('home.html', {'user': request.user})
	
def viewpost(request):
	if request.user.is_authenticated():
		#users = users.objects.get(id=int(request.user.id))
		selfposts = posts.objects.filter(user_id=1)
		variables = RequestContext(request, {'posts':selfposts,'users':users})
		return render_to_response('home.html', variables)
	else:
		return HttpResponseRedirect('/accounts/login/')
		
def newpost(request):
	validation = ''
	if request.user.is_authenticated():
		c = {}
		c.update(csrf(request))
		if request.method == 'POST': # If the form has been submitted...
			form = postForm(request.POST) # A form bound to the POST data
			validation='Form not Valid'
			#wow = [inputM,inputN,inputA,inputK,inputT]
			if form.is_valid():
				validation='Form Valid'
				inputUser = request.POST.get('user_id')
				inputHeader = request.POST.get('header')
				inputContent = request.POST.get('content')
				inputDate = request.POST.get('date')
				postssave = posts(None,inputUser,inputHeader,inputContent,inputDate)
				postssave.save()
				#form.save()
				
			return HttpResponseRedirect('/')
		else:
			form = postForm() # An unbound form
			postFormSet = formset_factory(postForm)		
			variables = RequestContext(request, {'form': form,'user': request.user,'users':users,'validation':validation,'c':c})
		return render_to_response('postform.html', variables)
	else:
		return HttpResponseRedirect('/accounts/login/') # Redirect after POST
		
def editpost(request,id):
	editpost = posts.objects.get(pk=id)
	validation = ''
	if request.user.is_authenticated():
		c = {}
		c.update(csrf(request))
		if request.method == 'POST': # If the form has been submitted...
			#form = postForm(request.POST) # A form bound to the POST data
			#validation='Form not Valid'
			edit = postForm(request.POST, instance=editpost)
			edit.save()
			#wow = [inputM,inputN,inputA,inputK,inputT]
			#form.save()
				
			return HttpResponseRedirect('/')
		else:
			form = postForm(instance=editpost) # An unbound form
			postFormSet = formset_factory(postForm)		
			variables = RequestContext(request, {'form': form,'user': request.user,'users':users,'validation':validation,'c':c})
		return render_to_response('postform.html', variables)
	else:
		return HttpResponseRedirect('/accounts/login/') # Redirect after POST
		
		
def deletepost(request,id):
	deletepost = posts.objects.get(pk= id)
	deletepost.delete()
	return HttpResponseRedirect('/')