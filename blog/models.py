from django.db import models
from django.contrib.auth.models import User
import datetime

class users(models.Model):
	user = models.ForeignKey(User, unique=True)
	def __unicode__(self):
		ID = str(self.id)
		return ID

class posts(models.Model):
	user_id = models.ForeignKey(users, null=False)
	header = models.CharField(max_length = 255, null=False)
	content = models.TextField()
	date = models.DateTimeField(auto_now=True)
	def __unicode__(self):
		return self.header

class comment(models.Model):
	user_id = models.ForeignKey(users, null=True)
	id_post = models.ForeignKey(posts, null=True)
	comment = models.TextField(null=True)
	date = models.DateTimeField(auto_now=True)
	def __unicode__(self):
		return self.comment