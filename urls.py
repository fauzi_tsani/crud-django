from django.conf.urls.defaults import *
from django.conf import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from CRUD.blog.views import index,viewpost,newpost,editpost,deletepost
from django.contrib.auth import views as authviews
admin.autodiscover()

urlpatterns = patterns('',
	# Example:
	# (r'^CRUD/', include('CRUD.foo.urls')),
	(r'^$', viewpost), #home
	(r'^newpost/$', newpost),
	(r'^edit/(?P<id>\w{0,50})/', editpost),
	(r'^delete/(?P<id>\w{0,50})/', deletepost),
	#(r'^CRUD/(P<postid>\d+)$', 'CRUD.blog.views.index'),#viewpost & comment
	
	# Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
	# to INSTALLED_APPS to enable admin documentation:
	# (r'^admin/doc/', include('django.contrib.admindocs.urls')),

	# Uncomment the next line to enable the admin:
	(r'^admin/', include(admin.site.urls)),
	(r'^accounts/', include('CRUD.accounts.urls')),
	url(r'^amnesia/$', authviews.password_reset),
	url(r'^amnesia/done/$', authviews.password_reset_done),
)
if settings.DEBUG: 
	urlpatterns += patterns('', 
		(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}), ) 